import React from 'react';
import {
  TableCell,
  TableHead,
  TableRow,
  TableSortLabel,
  TableContainer,
  Table,
  TableBody,
  Paper,
} from "@material-ui/core";
import PropTypes from 'prop-types';
import {withStyles, makeStyles} from "@material-ui/core/styles";

const headCells = [
  {id: 'no', numeric: true, disablePadding: true, label: 'No.'},
  {id: 'title', numeric: false, disablePadding: true, label: 'Title'},
  {id: 'description', numeric: false, disablePadding: true, label: 'Description'},
  {id: 'year', numeric: false, disablePadding: true, label: 'Year'},
  {id: 'duration', numeric: false, disablePadding: true, label: 'Duration'},
  {id: 'rating', numeric: false, disablePadding: true, label: 'Rating'},
  {id: 'genre', numeric: false, disablePadding: true, label: 'Genre'},
  {id: 'action', numeric: false, disablePadding: true, label: 'Action'},
];

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
}));

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

// Descending Comparator
function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

// getComparator
function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

// stableSort
function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

// START - TableHead
const EnhancedTableHead = ({onRequestSort, order, orderBy}) => {
  const classes = useStyles();
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    
    <TableHead>
      <TableRow>
        
        {headCells && headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            padding={headCell.disablePadding ? 'none' : 'default'}
            sortDirection={orderBy === headCell.id ? order : false}>
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  )
}

EnhancedTableHead.propTypes = {
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};
// END - TableHead
//
//--------------------------------------------------------------------------------


// START Table
const EnhancedTable = ({data, renderAction}) => {
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('calories');

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  return (
    <TableContainer component={Paper}>
      <Table
        aria-labelledby="tableTitle"
        aria-label="enhanced table"
      >
        <EnhancedTableHead
          order={order}
          orderBy={orderBy}
          onRequestSort={handleRequestSort}
          rowCount={data.length}
          headCells={headCells}
        />
        <TableBody>
          {stableSort(data, getComparator(order, orderBy))
            .map((row, index) => {

              return (
                <TableRow
                  hover
                  tabIndex={-1}
                  key={row.title}
                >
                  <TableCell>
                    {index + 1}
                  </TableCell>
                  <TableCell>
                    {row.title}
                  </TableCell>
                  <TableCell>{row.description}</TableCell>
                  <TableCell>{row.year}</TableCell>
                  <TableCell>{row.duration}</TableCell>
                  <TableCell>{row.rating}</TableCell>
                  <TableCell>{row.genre}</TableCell>
                  <TableCell>-</TableCell>
                  <TableCell>
                    {renderAction(row)}
                  </TableCell>
                </TableRow>
              );
            })}
          {!data && (
            <TableRow style={{height: '50vh'}}>
              <TableCell colSpan={6}/>
            </TableRow>
          )}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

export default EnhancedTable;