import React, { useContext, useState } from "react"
import {UserContext} from "../context/UserContext"

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
const Login = () =>{
  const [, setUser] = useContext(UserContext)
  const [input, setInput] = useState({username: "" , password: ""})

  const handleSubmit = (event) =>{
    event.preventDefault()
    if (input.username === "admin" && input.password === "admin"){
      setUser({username: input.username})
      localStorage.setItem("user", JSON.stringify({username: "admin", password: "admin"}))
    }else{
      alert("username dan password gagal")
    }
  }

  const handleChange = (event) =>{
    let value = event.target.value
    let name = event.target.name
    switch (name){
      case "username":{
        setInput({...input, username: value})
        break;
      }
      case "password":{
        setInput({...input, password: value})
        break;
      }
      default:{break;}
    }
  }



  return(
    <>
      <form onSubmit={handleSubmit}>
        <div>
        <TextField name="username" required id="standard-required" label="Required" defaultValue="Username"  onChange={handleChange} value={input.username}/>
        <br/>
        <TextField
          id="standard-password-input"
          label="Password"
          type="password"
          autoComplete="current-password"
          onChange={handleChange} value={input.password}
          name="password"
        />
        <br/><br/>
        <button>Login</button>
        </div>
      </form>
    </>
  )
}

export default Login