import React, {useState, useEffect} from "react"
import axios from "axios"
import "./Movies.css"

import {
  Typography,
  Button,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from '@material-ui/core';
import Table from '../layouts/Table';
import 'fontsource-roboto';

const Games = () => {
  const [open, setOpen] = React.useState(false);
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  const [games, setGames] = useState(null)
  const [input, setInput] = useState({
    name: "",
    platform: "",
    release: 2020,
    genre: "",
  })
  const [selectedId, setSelectedId] = useState(0)
  const [statusForm, setStatusForm] = useState("create")

  useEffect(() => {
    if (games === null) {
      axios.get(`https://www.backendexample.sanbersy.com/api/games`)
        .then(res => {
          setGames(res.data.map(el => {
            return {
              name: el.name,
              release: el.release,
              genre: el.genre,
              platform: el.platform
            }
          }))
        })
    }
  }, [games])

  const handleChange = (event) => {
    let typeOfInput = event.target.name

    switch (typeOfInput) {
      case "name": {
        setInput({...input, name: event.target.value});
        break
      }
      case "release": {
        setInput({...input, release: event.target.value});
        break
      }
      case "platform": {
        setInput({...input, platform: event.target.value});
        break
      }
      case "genre": {
        setInput({...input, genre: event.target.value});
        break
      }
      default: {
        break;
      }
    }
  }
  const handleSubmit = (event) => {
    // menahan submit
    event.preventDefault()

    let title = input.title
    console.log(input)

    if (title.replace(/\s/g, '') !== "") {
      if (statusForm === "create") {
        axios.post(`https://www.backendexample.sanbersy.com/api/games`, {
          name: input.name,
          platform: input.platform,
          release: input.release,
          genre: input.genre,
        })
          .then(res => {
            setGames([...games, {id: res.data.id, ...input}])
          })
      } else if (statusForm === "edit") {
        axios.put(`https://www.backendexample.sanbersy.com/api/games/${selectedId}`, {
          name: input.name,
          platform: input.platform,
          release: input.release,
          genre: input.genre,
        })
          .then(res => {
            let singleGame = games.find(el => el.id === selectedId)
            singleGame.name = input.name
            singleGame.release = input.release
            singleGame.genre = input.genre
            singleGame.platform = input.platform
            setGames([...games])
          })
      }

      setStatusForm("create")
      setSelectedId(0)
      setInput({
        name: "",
        platform: "",
        release: 2020,
        genre: ""

      })
    }

  }
  const Action = ({itemId}) => {
    const handleDelete = () => {
      let newGames = games.filter(el => el.id != itemId)

      axios.delete(`https://www.backendexample.sanbersy.com/api/games/${itemId}`)
        .then(res => {
          console.log(res)
        })

      setGames([...newGames])

    }

    const handleEdit = () => {
      let singleGame = games.find(x => x.id === itemId)
      setInput({
        name: singleGame.name,
        platform: singleGame.platform,
        release: singleGame.release,
        genre: singleGame.genre
      })
      setSelectedId(itemId)
      setStatusForm("edit")
    }

    return (
      <>
        <Button onClick={handleEdit} color="primary">
          Primary
        </Button>
        &nbsp;
        <Button onClick={handleDelete} color="secondary">
          Delete
        </Button>
      </>
    )
  }

  return (
    <>
      <Typography variant="h2" component="h2" gutterBottom>
        List Games
      </Typography>

      <input onChange={e => setGames(games.filter(f => f.name === e.target.value))} />
      <Table data={games || []} renderAction={item => (<Action itemId={item.id} />)} />

      <br/><br/>
      <form onSubmit={handleSubmit}>
        <div>
          <Button variant="outlined" color="primary" onClick={handleClickOpen}>
            FORM GAME MODAL
          </Button>
          <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">Movie Form</DialogTitle>
            <DialogContent>
              <TextField
                autoFocus
                margin="dense"
                id="name"
                name="name"
                label="Games Name"
                type="text"
                value={input.name}
                onChange={handleChange}
                fullWidth
              />

              <TextField
                autoFocus
                margin="dense"
                id="year"
                name="year"
                label="Release Year"
                type="number"
                value={input.year}
                max={2020} min={1980}
                onChange={handleChange}
                fullWidth
              />

              <TextField
                autoFocus
                margin="dense"
                id="genre"
                name="genre"
                label="Genre"
                type="number"
                value={input.genre}
                onChange={handleChange}
                fullWidth
              />

              <TextField
                autoFocus
                margin="dense"
                id="platform"
                name="platform"
                label="Platform"
                value={input.platform}
                onChange={handleChange}
                fullWidth
              />
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose} color="primary">
                Cancel
              </Button>
              <Button onClick={handleSubmit} color="primary">
                Submit
              </Button>
            </DialogActions>
          </Dialog>
        </div>
      </form>
      <br/><br/><br/><br/>

    </>
  );
}

export default Games