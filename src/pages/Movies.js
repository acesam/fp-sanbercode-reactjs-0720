import React, {useState, useEffect} from "react"
import axios from "axios"
import "./Movies.css"

import {
  Typography,
  Button,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from '@material-ui/core';
import Table from '../layouts/TableMovies';
import 'fontsource-roboto';

const Movies = () => {
  const [open, setOpen] = React.useState(false);
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  const [movies, setMovies] = useState(null)
  const [input, setInput] = useState({
    title: "",
    description: "",
    Year: 2020,
    duration: 0,
    rating: 0,
    genre: "",
  })
  const [selectedId, setSelectedId] = useState(0)
  const [statusForm, setStatusForm] = useState("create")

  useEffect(() => {
    if (movies === null) {
      axios.get(`https://www.backendexample.sanbersy.com/api/movies`)
        .then(res => {
          setMovies(res.data.map(el => {
            return {
              title: el.title,
              description: el.description,
              genre: el.genre,
              duration: el.duration,
              rating: el.rating,
              year: el.year
            }
          }))
        })
    }
  }, [movies])

  const handleChange = (event) => {
    let typeOfInput = event.target.name

    switch (typeOfInput) {
      case "title": {
        setInput({...input, title: event.target.value});
        break
      }
      case "description": {
        setInput({...input, description: event.target.value});
        break
      }
      case "rating": {
        setInput({...input, rating: event.target.value});
        break
      }
      case "genre": {
        setInput({...input, genre: event.target.value});
        break
      }
      case "year": {
        setInput({...input, year: event.target.value});
        break
      }
      case "duration": {
        setInput({...input, duration: event.target.value});
        break
      }
      default: {
        break;
      }
    }
  }
  const handleSubmit = (event) => {
    // menahan submit
    event.preventDefault()

    let title = input.title
    console.log(input)

    if (title.replace(/\s/g, '') !== "") {
      if (statusForm === "create") {
        axios.post(`https://www.backendexample.sanbersy.com/api/movies`, {
          title: input.title,
          description: input.description,
          year: input.year,
          genre: input.genre,
          duration: input.duration,
          rating: input.rating,
          
        })
          .then(res => {
            setMovies([...movies, {id: res.data.id, ...input}])
          })
      } else if (statusForm === "edit") {
        axios.put(`https://www.backendexample.sanbersy.com/api/movies/${selectedId}`, {
          title: input.title,
          description: input.description,
          year: input.year,
          genre: input.genre,
          rating: input.rating,
          duration: input.duration,
        })
          .then(res => {
            let singleMovie = movies.find(el => el.id === selectedId)
            singleMovie.title = input.title
            singleMovie.description = input.description
            singleMovie.genre = input.genre
            singleMovie.rating = input.rating
            singleMovie.duration = input.duration
            singleMovie.year = input.year
            
            setMovies([...movies])
          })
      }

      setStatusForm("create")
      setSelectedId(0)
      setInput({
        title: "",
        description: "",
        year: 2020,
        duration: 0,
        rating: 0,
        genre: ""

      })
    }

  }
  const Action = ({itemId}) => {
    const handleDelete = () => {
      let newMovies = movies.filter(el => el.id != itemId)

      axios.delete(`https://www.backendexample.sanbersy.com/api/movies/${itemId}`)
        .then(res => {
          console.log(res)
        })

      setMovies([...newMovies])

    }

    const handleEdit = () => {
      let singleMovie = movies.find(x => x.id === itemId)
      setInput({
        title: singleMovie.title,
        description: singleMovie.description,
        year: singleMovie.year,
        genre: singleMovie.genre,
        duration: singleMovie.duration,
        rating: singleMovie.rating
      })
      setSelectedId(itemId)
      setStatusForm("edit")
    }

    return (
      <>
        <Button onClick={handleEdit} color="primary">
          Primary
        </Button>
        &nbsp;
        <Button onClick={handleDelete} color="secondary">
          Delete
        </Button>
      </>
    )
  }

  return (
    <>
      <Typography variant="h2" component="h2" gutterBottom>
        List Movies
      </Typography>

      <input onChange={e => setMovies(movies.filter(f => f.name === e.target.value))} />
      <Table data={movies || []} renderAction={item => (<Action itemId={item.id} />)} />

      <br/><br/>
      <form onSubmit={handleSubmit}>
        <div>
     <Button variant="outlined" color="primary" onClick={handleClickOpen}>
              FORM MOVIES MODAL
        </Button>
        
        
        <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
          <DialogTitle id="form-dialog-title">Movie Form</DialogTitle>
          <DialogContent>
            <TextField
              autoFocus
              margin="dense"
              id="title"
              name="title"
              label="Title"
              type="text"
              value={input.title} 
              onChange={handleChange}
              fullWidth
            />

            <TextField
              autoFocus
              margin="dense"
              id="description"
              name="description"
              label="Description"
              type="text"
              value={input.description} 
              onChange={handleChange}
              fullWidth
            />

            <TextField
              autoFocus
              margin="dense"
              id="year"
              name="year"
              label="Year"
              type="number"
              value={input.year} 
              max={2020} min={1980}
              onChange={handleChange}
              fullWidth
            />

            <TextField
              autoFocus
              margin="dense"
              id="duration"
              name="duration"
              label="Duration"
              type="number"
              value={input.duration} 
              onChange={handleChange}
              fullWidth
            />

            <TextField
              autoFocus
              margin="dense"
              id="genre"
              name="genre"
              label="Genre"
              type="text"
              value={input.genre} 
              onChange={handleChange}
              fullWidth
            />

            <TextField
              autoFocus
              margin="dense"
              id="rating"
              name="rating"
              label="Rating"
              type="number"
              value={input.rating} 
              onChange={handleChange}
              max={10} min={0}
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={handleSubmit} color="primary">
              Submit
            </Button>
          </DialogActions>
        </Dialog>
        
      </div>
    </form>
    <br/><br/><br/><br/>

    </>
  );
}

export default Movies