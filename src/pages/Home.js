import React, {Component} from "react"
import axios from "axios"

import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import 'fontsource-roboto';




function minuteToHours(num){
  var hours = (num / 60);
  var rhours = Math.floor(hours);
  var minutes = (hours - rhours) * 60;
  var rminutes = Math.round(minutes);
  return ( rhours === 0 ? "" : rhours + " Jam") + (rminutes === 0 ? "" : " " + rminutes + " Menit")
}

class Home extends Component {
  constructor(props){
    super(props)
    this.state = {
      movies: []
    }
  }

  componentDidMount(){
    axios.get(`https://www.backendexample.sanbersy.com/api/movies`)
    .then(res => {
      let movies = res.data.map(el=>{ return {
        id: el.id, 
        title: el.title, 
        rating: el.rating,
        duration: el.duration,
        genre: el.genre,
        description: el.description
      }})
      this.setState({movies})
    })
  }

  render(){
    return (
      <>
       <Typography variant="h2" component="h2" gutterBottom>
        Best Movies Chart
      </Typography>
      {
            this.state.movies.map((item)=>{
              return(
          
       <Card>
        <CardActionArea>
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
            {item.title} {minuteToHours(item.duration)}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
            {item.description}
            </Typography>
          </CardContent>
        </CardActionArea>
        <CardActions>
        <Button size="small" color="primary">
          <b>Genre: </b> {item.genre}
        </Button>
        <Button size="small" color="primary">
          <b>Rating: </b> {item.rating}
        </Button>
      </CardActions>
    </Card>
      )
    })
  }

      </>
    )
  }
}

export default Home