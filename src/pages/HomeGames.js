import React, {Component} from "react"
import axios from "axios"

import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import 'fontsource-roboto';

class Home extends Component {
  constructor(props){
    super(props)
    this.state = {
      movies: []
    }
  }

  componentDidMount(){
    axios.get(`https://www.backendexample.sanbersy.com/api/games`)
    .then(res => {
      let movies = res.data.map(el=>{ return {
        id: el.id, 
        name: el.name, 
        platform: el.platform,
        release: el.release,
        genre: el.genre,
        image: el.image_url
      }})
      this.setState({movies})
    })
  }

  render(){
    return (
      <>
       <Typography variant="h2" component="h2" gutterBottom>
        Best Games Chart
        </Typography>
        {
              this.state.movies.map((item)=>{
                return(
            
        <Card>
          <CardActionArea>
            <CardMedia
              component="img"
              alt="Games Image"
              height="250"
              image={item.image}
              title="Best Games for SanberCode"
            />
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
              {item.name} {item.release}
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
              {item.description}
              </Typography>
            </CardContent>
          </CardActionArea>
          <CardActions>
          <Button size="small" color="primary">
            <b>Genre: </b> {item.genre}
          </Button>
          <Button size="small" color="primary">
            <b>Platform: </b> {item.platform}
          </Button>
        </CardActions>
      </Card>
        )
      })
    }
      </>
    )
  }
}

export default Home